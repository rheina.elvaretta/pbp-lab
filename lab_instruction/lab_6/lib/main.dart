import 'package:flutter/material.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: WelcomeScreen(),
    );
  }
}

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 50),
        Text(
          'Selamat Datang di',
          style: TextStyle(
            fontSize: 27,
            fontFamily: 'Inter',
            fontWeight: FontWeight.w400,
            color: Color.fromRGBO(0, 0, 0, 100),
            decoration: TextDecoration.none,
          ),
          textAlign: TextAlign.center,
        ),
        Text(
          'FORUM!',
          style: TextStyle(
            fontSize: 57,
            fontWeight: FontWeight.bold,
            color: Color.fromRGBO(3, 68, 96, 100),
            fontFamily: 'Inter',
            decoration: TextDecoration.none,
          ),
          textAlign: TextAlign.center,
        ),
        new Image.asset('assets/images/illustration.png',
            width: 500, height: 300),
        Text(
          'Forum ini berisi cerita, saran, dan pengalaman dari berbagai kalangan untuk membantu dan menyemangati teman-teman dalam menghadapi pandemi COVID!',
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.w300,
            color: Color.fromRGBO(0, 0, 0, 100),
            fontFamily: 'Inter',
            decoration: TextDecoration.none,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 40),
        TextButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(
                  Color.fromRGBO(3, 68, 96, 100)),
              padding:
                  MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(20)),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ))),
          onPressed: () {},
          child: Text(
            'ADD FORUM',
            style: TextStyle(
              color: Color.fromRGBO(255, 255, 255, 100),
              fontSize: 14,
              fontWeight: FontWeight.bold,
              fontFamily: 'Inter',
              decoration: TextDecoration.none,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    ));
  }
}