import 'dart:html';

import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => new _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController controllerFrom = new TextEditingController();
  TextEditingController controllerMessage = new TextEditingController();
  
  // void tampilkanData(){
  //   AlertDialog alertDialog = new AlertDialog(
  //     content: new Container(
  //       height: 200,
  //       child: new Column(
  //         children:<Widget> [
  //           new Text("From: ${controllerFrom.text}"),
  //           new Text("From: ${controllerMessage.text}"),
  //         ],
  //       ),
  //     ),
  //   );
  //   showDialog(context: context, child: alertDialog);
  // }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.only(top: 60, left: 16, right: 16),
          child: SingleChildScrollView(
            child: Form(
              child: Column(
                children: [
                  Image.asset('assets/images/illustration.png'),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Selamat Datang di',
                    style: TextStyle(
                      fontSize: 17,
                      fontFamily: 'Inter',
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(0, 0, 0, 100),
                      decoration: TextDecoration.none,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'FORUM!',
                    style: TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(3, 68, 96, 100),
                      fontFamily: 'Inter',
                      decoration: TextDecoration.none,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Forum ini berisi cerita, saran, dan pengalaman dari berbagai kalangan untuk membantu dan menyemangati teman-teman dalam menghadapi pandemi COVID!',
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      color: Color.fromRGBO(0, 0, 0, 100),
                      fontFamily: 'Inter',
                      decoration: TextDecoration.none,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: controllerFrom,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      labelText: 'From',
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: controllerMessage,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      labelText: 'Message',
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints.tightFor(),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        backgroundColor: MaterialStateProperty.all(
                          Color.fromRGBO(3, 68, 96, 100),
                        ),
                      ),
                      child: Text(
                        'ADD FORUM',
                        style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 100),
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Inter',
                          decoration: TextDecoration.none,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      onPressed: () {
                        showDialog(
                        context: context, 
                        builder: (context) => AlertDialog(
                          content: new Container(
                            height: 100,
                            child: new Column(
                              children:<Widget> [
                                new Text("From: ${controllerFrom.text}"),
                                new Text("Message: ${controllerMessage.text}"),
                              ],
                            ),
                          ),
                          actions: [
                            TextButton(onPressed: (){
                              Navigator.pop(context);
                            }, child: Text('ok')),
                          ],
                        ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      )
    );
  }
}

// class _BelajarFormState extends State<BelajarForm> {
//   final _formKey = GlobalKey<FormState>();

//   double nilaiSlider = 1;
//   bool nilaiCheckBox = false;
//   bool nilaiSwitch = true;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("BelajarFlutter.com"),
//       ),
//       body: Form(
//         key: _formKey,
//         child: SingleChildScrollView(
//           child: Container(
//             padding: EdgeInsets.all(20.0),
//             child: Column(
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: TextFormField(
//                     decoration: new InputDecoration(
//                       hintText: "contoh: Susilo Bambang",
//                       labelText: "Nama Lengkap",
//                       icon: Icon(Icons.people),
//                       border: OutlineInputBorder(
//                           borderRadius: new BorderRadius.circular(5.0)),
//                     ),
//                     validator: (value) {
//                       if (value.isEmpty) {
//                         return 'Nama tidak boleh kosong';
//                       }
//                       return null;
//                     },
//                   ),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: TextFormField(
//                     obscureText: true,
//                     decoration: new InputDecoration(
//                       labelText: "Password",
//                       icon: Icon(Icons.security),
//                       border: OutlineInputBorder(
//                           borderRadius: new BorderRadius.circular(5.0)),
//                     ),
//                     validator: (value) {
//                       if (value.isEmpty) {
//                         return 'Password tidak boleh kosong';
//                       }
//                       return null;
//                     },
//                   ),
//                 ),
//                 CheckboxListTile(
//                   title: Text('Belajar Dasar Flutter'),
//                   subtitle: Text('Dart, widget, http'),
//                   value: nilaiCheckBox,
//                   activeColor: Colors.deepPurpleAccent,
//                   onChanged: (value) {
//                     setState(() {
//                       nilaiCheckBox = value;
//                     });
//                   },
//                 ),
//                 SwitchListTile(
//                   title: Text('Backend Programming'),
//                   subtitle: Text('Dart, Nodejs, PHP, Java, dll'),
//                   value: nilaiSwitch,
//                   activeTrackColor: Colors.pink[100],
//                   activeColor: Colors.red,
//                   onChanged: (value) {
//                     setState(() {
//                       nilaiSwitch = value;
//                     });
//                   },
//                 ),
//                 Slider(
//                   value: nilaiSlider,
//                   min: 0,
//                   max: 100,
//                   onChanged: (value) {
//                     setState(() {
//                       nilaiSlider = value;
//                     });
//                   },
//                 ),
//                 RaisedButton(
//                   child: Text(
//                     "Submit",
//                     style: TextStyle(color: Colors.white),
//                   ),
//                   color: Colors.blue,
//                   onPressed: () {
//                     if (_formKey.currentState.validate()) {}
//                   },
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }