from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    NAME = models.CharField(primary_key=True,max_length=30)
    NPM = models.CharField(max_length=10)
    DOB = models.DateField(null = True)
    # TODO Implement missing attributes in Friend model
