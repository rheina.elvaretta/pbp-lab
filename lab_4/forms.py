from lab_2.models import Note
from django import forms

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['NoteTo','NoteFrom','NoteTitle','NoteMessage']


    input_attrs_to = {
        'type' : 'text',
        'placeholder' : 'Reciever', 
    }

    input_attrs_from = {
        'type' : 'text',
        'placeholder' : 'Sender'
    }

    input_attrs_title = {
        'type' : 'text',
        'placeholder' : 'Title', 
    }

    input_attrs_message = {
        'type' : 'text',
        'placeholder' : 'Message'
    }

    NoteTo = forms.CharField(label='', required=True, max_length=250, 
    widget= forms.TextInput(attrs=input_attrs_to))

    NoteFrom = forms.CharField(label='', required=True, max_length=250, 
    widget= forms.TextInput(attrs=input_attrs_from))

    NoteTitle = forms.CharField(label='', required=True, max_length=250, 
    widget= forms.TextInput(attrs=input_attrs_title))

    NoteMessage = forms.CharField(label='', required=True, max_length=250, 
    widget= forms.TextInput(attrs=input_attrs_message))