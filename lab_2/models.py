from django.db import models

# Create your models here.
class Note(models.Model):
    NoteTo = models.CharField(max_length=250)
    NoteFrom = models.CharField(max_length=250)
    NoteTitle = models.CharField(max_length=250)
    NoteMessage = models.CharField(max_length=250)
    
