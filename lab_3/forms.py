from lab_1.models import Friend
from django import forms

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['NAME','NPM','DOB']


    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Your Name', 
    }

    input_attrs_npm = {
        'type' : 'text',
        'placeholder' : 'Your NPM'
    }

    date_attrs = {
        'type' : 'date',
    }

    NAME = forms.CharField(label='', required=True, max_length=27, 
    widget= forms.TextInput(attrs=input_attrs))

    NPM = forms.CharField(label='', required=True, max_length=10, 
    widget= forms.TextInput(attrs=input_attrs_npm))

    DOB = forms.DateField(label='',required=True,
    widget=forms.DateInput(attrs=date_attrs))