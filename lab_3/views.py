from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required(login_url='/admin/login/')

def index(request):
    friends = Friend.objects.all() 
    response = {'friends': friends}
    return render(request, 'lab3_index.html',response)

def add_friend(request):
    form = FriendForm(request.POST or None)
    if form.is_valid and request.method == 'POST':
        form.save()
        return HttpResponseRedirect('/lab-3')
    context = {'form' : form}
    return render(request, 'lab3_forms.html', context)